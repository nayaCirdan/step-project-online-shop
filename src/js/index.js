import $ from 'jquery';
import '../../node_modules/bootstrap/js/dist/carousel'
import '../../node_modules/bootstrap/js/dist/collapse.js'
import '../../node_modules/bootstrap/js/dist/modal.js'
import '../../node_modules/owl.carousel/dist/owl.carousel'
import '../../node_modules/bootstrap/js/dist/scrollspy.js'
import '../../node_modules/bootstrap/js/dist/tab'
import '../../node_modules/@fortawesome/fontawesome-free/js/all'
import '../../node_modules/popper.js/dist/popper'
import '../../node_modules/bootstrap/js/dist/tooltip'
import '../../node_modules/owl.carousel/dist/owl.carousel'


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

//BURGER-CLOSE BTN
$('.nav__burger').on('click', () => {
    if (!($('.nav__burger').hasClass('.collapsed'))) {
        $('.burger__icon').toggleClass('active');
    }
});
//----------------

//CART INPUT
$(function () {
    (function quantityProducts() {
        const $quantityArrowMinus = $(".quantity__btn--minus");
        const $quantityArrowPlus = $(".quantity__btn--plus");
        const $quantityNum = $(".quantity__counter");
        $quantityArrowMinus.click(quantityMinus);
        $quantityArrowPlus.click(quantityPlus);

        function quantityMinus() {
            if ($quantityNum.val() > 1) {
                $quantityNum.val(+$quantityNum.val() - 1);
            }
        }
        function quantityPlus() {
            $quantityNum.val(+$quantityNum.val() + 1);
        }
    })();
});
//----------

//CART MODAL EMPTY/FULL
const cartCounter = document.querySelector('.cart__cart-counter');
const cartModalCaller = document.querySelector('.top-menu__cart');

if (cartCounter.innerText === '0') {
    cartModalCaller.setAttribute('data-target', '#cartEmptyModal');
} else {
    cartModalCaller.setAttribute('data-target', '#cartFullModal');
}
//----------




//HEADER_NAV_SCROLLING

const navToScroll=$('.header__nav-item');

navToScroll.on ('click','a',function (ev) {
    ev.preventDefault();
    console.log(ev);
    let sectionLink=$(this).attr('href');
    let sectionOffset=$(sectionLink).offset().top;
    console.log(sectionOffset);
    $(document.body.parentElement).animate({scrollTop: sectionOffset},1000);

});

/*$('body').scrollspy({
    target: '#navbar-example',
    offset:50
});*/
/*$(window).on('scroll',()=>{
    let navLink=document.querySelector('.nav-item__nav-link');
    if(navLink.classList.contains('active')){
        $(navLink.parentElement).addClass('active');
    }else{
        $(navLink.parentElement).removeClass('active');
    }*/
/*    console.log(navLinkActive);
    console.log(navLinkActive.parentElement);
    $(navLinkActive.parentElement).addClass('active');*/
/*});*/

$(document).ready(function(){
    $('.owl-carousel-bottom').owlCarousel({
        loop:true,
        margin:5,
        URLhashListener:true,
        startPosition: 'slide-1',
        dots: false,
        stagePadding: 0,
        responsive: {
            0: {
                items:3
            },
            480:{
                items:5
            },
            768 : {
                items:6
            }
        }
        }
    )
});
$(document).ready(function(){
    $('.owl-carousel-top').owlCarousel({
            loop:true,
            items:1,
            margin:10,
            dots: false,
            stagePadding: 0,
        }
    );
    $('.control-next').click(function (e) {
        e.preventDefault();
        $('.owl-carousel-bottom').trigger("next.owl.carousel");
        $('.owl-carousel-top').trigger("next.owl.carousel");
    });
    $('.control-prev').click(function (e) {
        e.preventDefault();
        $('.owl-carousel-bottom').trigger("prev.owl.carousel");
        $('.owl-carousel-top').trigger("prev.owl.carousel");
    });

});

