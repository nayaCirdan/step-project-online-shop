const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    autoPrefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    del = require('del'),
    sourceMaps = require('gulp-sourcemaps'),
    webpackStream = require('webpack-stream'),
    rename = require('gulp-rename'),
    gulpIf = require('gulp-if'),
    imageMin = require('gulp-imagemin'),
    webpack = require('webpack');
// pug = require("gulp-pug"),

let isDev = true;
let isProd = !isDev;

gulp.task('jsProcess', () => {
    return gulp
        .src('./src/js/index.js')
        .pipe(
            webpackStream({
                output: {
                    filename: 'script.js',
                },
                module: {
                    rules: [
                        {
                            test: /\.js$/,
                            exclude: /(node_modules)/,
                            loader: 'babel-loader',
                            query: {
                                presets: ['@babel/preset-env'],
                            },
                        },
                    ],
                },
                plugins: [
                    new webpack.ProvidePlugin({
                        $: 'jquery',
                        jQuery: 'jquery',
                        'window.jQuery': 'jquery'
                    }),
                ],
                mode: isDev ? 'development' : 'production',
                devtool: isDev ? 'eval-source-map' : 'none',
            }),
        )
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
});

// gulp.task("pugProcess", () => {
//   return gulp
//     .src("./src/index.pug")
//     .pipe(
//       pug({
//         pretty: true
//       })
//     )
//     .pipe(gulp.dest("./dist"))
//     .pipe(browserSync.stream());
// });

gulp.task('cssProcess', () => {
    return gulp
        .src('./src/sass/**/*.scss')
        .pipe(gulpIf(isDev, sourceMaps.init()))
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(concat('styles.css'))
        .pipe(
            autoPrefixer({
                overrideBrowserslist: ['last 2 versions'],
                cascade: false,
            }),
        )
        .pipe(
            gulpIf(
                isProd,
                cleanCSS({
                    level: 2,
                }),
            ),
        )
        .pipe(gulpIf(isDev, sourceMaps.write('./maps', { addComment: false })))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('imgProcess', () => {
    return gulp
        .src('./src/img/**/*')
        .pipe(
            gulpIf(
                isProd,
                imageMin(
                    [
                        imageMin.gifsicle({ interlaced: true }),
                        imageMin.mozjpeg({ quality: 75, progressive: true }),
                        imageMin.optipng({ optimizationLevel: 5 }),
                        imageMin.svgo({
                            plugins: [
                                { removeViewBox: true },
                                { cleanupIDs: false },
                            ],
                        }),
                    ],
                    {
                        verbose: true,
                    },
                ),
            ),
        )
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream());
});

gulp.task('transferFonts', () => {
    return gulp
        .src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(browserSync.stream());
});

gulp.task('distClean', () => {
    return del(['./dist/*']);
});

gulp.task('dev', () => {
    browserSync.init({
        server: {
            baseDir: './',
            index: 'index.html',
        },
        notify: false,
    });
    gulp.watch('./src/fonts/**/*', gulp.series('transferFonts')),
        gulp.watch('./src/sass/**/*', gulp.series('cssProcess')),
        gulp.watch('./src/js/**/*', gulp.series('jsProcess')),
        gulp.watch('./src/img/**/*', gulp.series('imgProcess')),
        // gulp.watch(
        //   "./src/index.pug",
        //   gulp.series("pugProcess")
        // );
        gulp.watch('./index.html').on('change', browserSync.reload);
});

gulp.task(
    'build',
    gulp.series(
        'distClean',
        gulp.parallel(
            'cssProcess',
            'jsProcess',
            'imgProcess',
            'transferFonts',
            // "pugProcess",
        ),
    ),
);
